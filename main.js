/* main.js */
console.log("Welcome tic tac toe")

var cells = document.getElementsByClassName('cell');

[].forEach.call(cells, function(el){
  el.addEventListener("click", whenClick);
  el.addEventListener("dblclick", whenDoubleCLick)
});

function whenClick(){
  this.innerHTML = "X";
}

function whenDoubleCLick(){
  this.innerHTML = "O";}
